﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JsonSerialiser
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine($"{{ \"fixtureid\": {args[0]}, \"datetime\":\"{DateTime.Now}\", \"callcnt\": {args[1]}}}");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ProcessCaller
{
    class Program
    {
        const string mainurl = "http://localhost:4000";

        static void Main(string[] args)
        {
            var http = new HttpClient();
            var upload = $"{mainurl}/apps";

            var soccer = "SoccerModel";
            var cricket = "CricketModel";
            var tennis = "TennisModel";
            var app = "JsonSerialiser.exe";
            UploadApp(http, upload, app, soccer).Wait();
            UploadApp(http, upload, app, cricket).Wait();
            UploadApp(http, upload, app, tennis).Wait();

            Console.WriteLine("Sent all app mappings. Press a key to start sending.");
            Console.ReadKey();
            var tasks = CreateCallers(1, 3, soccer)
                .Union(CreateCallers(1000, 3, cricket))
                .Union(CreateCallers(2000, 3, tennis))
                .ToArray();
            Console.WriteLine("Press a key to end");
            Console.ReadKey();
        }

        private static Task UploadApp(HttpClient http, string upload, string app, string appName)
        {
            var headerApp = Path.Combine(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location), app);
            var content = new StringContent("soccer");
            content.Headers.Add("name", appName);
            content.Headers.Add("app", headerApp);
            Console.WriteLine($"Set {appName} - {headerApp}");
            return http.PostAsync(upload, content);
        }

        public static IEnumerable<Task> CreateCallers(int offset, int number, string name)
        {
            Console.WriteLine($"Starting fixture ids from {offset} to {offset+number} to call {name}");
            for (int i = offset; i < number + offset; i++)
            {
                var num = i;
                yield return Task.Factory.StartNew(
                    () =>
                    {
                        var read = new Uri( $"{mainurl}/{name}/{num}");
                        var client = new WebClient();
                        long c = 0;
                        while (true)
                        {
                            try
                            {
                                var ret = client.DownloadString(read);
                                if(c % 100 == 0)
                                {
                                    Console.WriteLine(ret);
                                }
                                c++;
                            }
                            catch
                            {
                                Console.WriteLine($"{num} - {name} - {DateTime.Now} - Error ");
                            }
                        }
                    }, CancellationToken.None, TaskCreationOptions.LongRunning, TaskScheduler.Default);
            }
        }
    }
}

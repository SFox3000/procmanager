defmodule ProcessManager.Apps do
  require Logger
  def start_link() do
    Agent.start_link(fn -> Map.new end, name: __MODULE__)
  end


def add_app(app, path) do
   __MODULE__
    |> Agent.update(fn apps -> 
        Logger.info("Adding> #{app}")
        Map.put(apps, app, path)
        
     end)
end

 def get_app(app) do
  Logger.info("Getting> #{app}")
    __MODULE__
      |> Agent.get(fn apps ->
           apps[app]
         end)
   end

end
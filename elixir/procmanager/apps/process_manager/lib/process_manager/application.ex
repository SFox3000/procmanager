defmodule ProcessManager.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    import Supervisor.Spec, warn: false
    # List all child processes to be supervised
    children = [
      supervisor(Registry, [:unique, :fixture_registry_name])
      # Starts a worker by calling: ProcessManager.Worker.start_link(arg)
      # {ProcessManager.Worker, arg},
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: ProcessManager.Supervisor]
    Supervisor.start_link(children, opts)
    ProcessManager.Apps.start_link()
    ProcessManager.ProcessSupervisor.start_link()
  end
end

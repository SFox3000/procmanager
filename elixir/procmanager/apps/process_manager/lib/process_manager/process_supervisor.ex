defmodule ProcessManager.ProcessSupervisor do
  use Supervisor
  require Logger


  @fixture_registry_name :fixture_registry_name

   def start_link, do: Supervisor.start_link(__MODULE__, [], name: __MODULE__)


  def find_or_create_process(fixture_id, appname) when is_integer(fixture_id) do
    if fixture_process_exists?(fixture_id) do
      {:ok, fixture_id}
    else
      fixture_id |> create_fixture_process appname
    end
  end

  def fixture_process_exists?(fixture_id) when is_integer(fixture_id) do
    case Registry.lookup(@fixture_registry_name, fixture_id) do
      [] -> false
      _ -> true
    end
  end

  def create_fixture_process(fixture_id,appname) when is_integer(fixture_id) do
    case Supervisor.start_child(__MODULE__, [fixture_id, appname]) do
      {:ok, _pid} -> {:ok, fixture_id}
      {:error, {:already_started, _pid}} -> {:error, :process_already_exists}
      other -> {:error, other}
    end
  end


   def fixture_process_count, do: Supervisor.which_children(__MODULE__) |> length


  @doc """
  Return a list of `fixture_id` integers known by the registry.

  ex - `[1, 23, 46]`
  """
  def fixture_ids do
    Supervisor.which_children(__MODULE__)
    |> Enum.map(fn {_, fixture_proc_pid, _, _} ->
      Registry.keys(@fixture_registry_name, fixture_proc_pid)
      |> List.first
    end)
    |> Enum.sort
  end

  @doc false
  def init(_) do
    children = [
      worker(ProcessManager.Fixture, [], restart: :temporary)
    ]

    # strategy set to `:simple_one_for_one` to handle dynamic child processes.
    supervise(children, strategy: :simple_one_for_one)
  end

end
defmodule ProcessManager.Fixture do
  use GenServer
  require Logger
  alias Porcelain.Result


  @fixture_registry_name :fixture_registry_name
  @process_lifetime_ms 60_000 

  defstruct fixture_id: 0,
            calls: 0,
            procestype: nil,
            timer_ref: nil

  def start_link(fixture_id, appname) when is_integer(fixture_id) do
    GenServer.start_link(__MODULE__, [fixture_id, appname], name: via_tuple(fixture_id))
  end


  # registry lookup handler
  defp via_tuple(fixture_id), do: {:via, Registry, {@fixture_registry_name, fixture_id}}

  def make_call(fixture_id) do
    GenServer.call(via_tuple(fixture_id), :make_call)
  end

  @doc """
  Returns the pid for the `fixture_id` stored in the registry
  """
  def whereis(fixture_id) do
    case Registry.lookup(@fixture_registry_name, fixture_id) do
      [{pid, _}] -> pid
      [] -> nil
    end
  end


  @doc """
  Init callback
  """
  def init([fixture_id, appname]) do
    # Add a msg to the process mailbox to
    # tell this process to run `:fetch_data`
    send(self(), %{fetch_data: appname, fixture_id: fixture_id})
    send(self(), :set_terminate_timer)

    Logger.info("Process created... Fixture ID: #{fixture_id}")
    Logger.info("Process created... Appname: #{appname}")

    # Set initial state and return from `init`
    {:ok, %__MODULE__{ fixture_id: fixture_id }}
  end

  def handle_info(%{fetch_data: appname, fixture_id: fixture_id}, state) do
    proc = ProcessManager.Apps.get_app(appname)
    updated_state = %__MODULE__{ state | procestype: proc, fixture_id: fixture_id }

    {:noreply, updated_state}
  end

  def handle_info(:set_terminate_timer, %__MODULE__{ timer_ref: nil } = state) do
    updated_state =
      %__MODULE__{ state | timer_ref: Process.send_after(self(), :end_process, @process_lifetime_ms) }

    {:noreply, updated_state}
  end

  def handle_info(:set_terminate_timer, %__MODULE__{ timer_ref: timer_ref } = state) do
    timer_ref |> Process.cancel_timer

    updated_state =
      %__MODULE__{ state | timer_ref: Process.send_after(self(), :end_process, @process_lifetime_ms) }

    {:noreply, updated_state}
  end

  def handle_info(:end_process, state) do
    Logger.info("Process terminating... Fixture ID: #{state.fixture_id}")
    {:stop, :normal, state}
  end

  @doc false
  def handle_call(:make_call, _from, state) do
    Logger.info("Process call... proc is #{state.procestype}")
    %Result{out: output, status: status} = Porcelain.shell("#{state.procestype} #{state.fixture_id} #{state.calls}")
    {:reply, output, %__MODULE__{ state | calls: state.calls + 1 }}
  end

end
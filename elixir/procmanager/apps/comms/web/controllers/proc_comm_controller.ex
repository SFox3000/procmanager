defmodule Comms.ProcCommController do
  use Comms.Web, :controller

  def process(conn, %{"mservice" => mservice, "fixtureid" => stringfixtureid}) do
    {fixtureid, _} = Integer.parse(stringfixtureid)
    ProcessManager.ProcessSupervisor.find_or_create_process(fixtureid, mservice)
    data = ProcessManager.Fixture.make_call(fixtureid)
    text conn, data
  end

  def add_app(conn, _parameters) do
    [apps] = conn |> get_req_header("name")
    [proc] = conn |> get_req_header("app")
    ProcessManager.Apps.add_app(apps, proc)
    text conn, "Done"
  end
end

defmodule Comms.Router do
  use Comms.Web, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", Comms do
    pipe_through :api
    post "/apps", ProcCommController, :add_app
    get "/:mservice/:fixtureid", ProcCommController, :process
  end
end

defmodule Comms.ProcCommControllerTest do
  use Comms.ConnCase

  alias Comms.ProcComm
  @valid_attrs %{}
  @invalid_attrs %{}

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  test "lists all entries on index", %{conn: conn} do
    conn = get conn, proc_comm_path(conn, :index)
    assert json_response(conn, 200)["data"] == []
  end

  test "shows chosen resource", %{conn: conn} do
    proc_comm = Repo.insert! %ProcComm{}
    conn = get conn, proc_comm_path(conn, :show, proc_comm)
    assert json_response(conn, 200)["data"] == %{"id" => proc_comm.id}
  end

  test "renders page not found when id is nonexistent", %{conn: conn} do
    assert_error_sent 404, fn ->
      get conn, proc_comm_path(conn, :show, -1)
    end
  end

  test "creates and renders resource when data is valid", %{conn: conn} do
    conn = post conn, proc_comm_path(conn, :create), proc_comm: @valid_attrs
    assert json_response(conn, 201)["data"]["id"]
    assert Repo.get_by(ProcComm, @valid_attrs)
  end

  test "does not create resource and renders errors when data is invalid", %{conn: conn} do
    conn = post conn, proc_comm_path(conn, :create), proc_comm: @invalid_attrs
    assert json_response(conn, 422)["errors"] != %{}
  end

  test "updates and renders chosen resource when data is valid", %{conn: conn} do
    proc_comm = Repo.insert! %ProcComm{}
    conn = put conn, proc_comm_path(conn, :update, proc_comm), proc_comm: @valid_attrs
    assert json_response(conn, 200)["data"]["id"]
    assert Repo.get_by(ProcComm, @valid_attrs)
  end

  test "does not update chosen resource and renders errors when data is invalid", %{conn: conn} do
    proc_comm = Repo.insert! %ProcComm{}
    conn = put conn, proc_comm_path(conn, :update, proc_comm), proc_comm: @invalid_attrs
    assert json_response(conn, 422)["errors"] != %{}
  end

  test "deletes chosen resource", %{conn: conn} do
    proc_comm = Repo.insert! %ProcComm{}
    conn = delete conn, proc_comm_path(conn, :delete, proc_comm)
    assert response(conn, 204)
    refute Repo.get(ProcComm, proc_comm.id)
  end
end

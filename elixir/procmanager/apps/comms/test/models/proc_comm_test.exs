defmodule Comms.ProcCommTest do
  use Comms.ModelCase

  alias Comms.ProcComm

  @valid_attrs %{}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = ProcComm.changeset(%ProcComm{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = ProcComm.changeset(%ProcComm{}, @invalid_attrs)
    refute changeset.valid?
  end
end
